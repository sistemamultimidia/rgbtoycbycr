package img;
/**
 * Y = I;
A = [.229 .587 .114 ; -.168736 -.331264 .5 ; .5 -.418688 -.081312];
Y(:,:,1) = A(1,1)*I(:,:,1) + A(1,2)*I(:,:,2) + A(1,3)*I(:,:,3);
Y(:,:,2) = A(2,1)*I(:,:,1) + A(2,2)*I(:,:,2) + A(2,3)*I(:,:,3) + 128;
Y(:,:,3) = A(3,1)*I(:,:,1) + A(3,2)*I(:,:,2) + A(3,3)*I(:,:,3) + 128;

% Let's use MATLAB's inbuilt convert (because of the normalizations):
Y = rgb2ycbcr(I);
 * 
 * 
 * */

	public class ColorConverter {

	    private ColorConverter() {}
	    //https://www.itu.int/rec/R-REC-BT/en
	    public static enum YCbCrColorSpace {ITU_BT_601,ITU_BT_709_HDTV};
	    
	    //https://en.wikipedia.org/wiki/CIE_1931_color_space
	    // X2, Y2, Z2
	    public static float[] CIE2_A = {109.850f, 100f, 35.585f}; //Incandescent
	    public static float[] CIE2_C = {98.074f, 100f, 118.232f};
	    public static float[] CIE2_D50 = {96.422f, 100f, 82.521f};
	    public static float[] CIE2_D55 = {95.682f, 100f, 92.149f};
	    public static float[] CIE2_D65 = {95.047f, 100f, 108.883f}; //Daylight
	    public static float[] CIE2_D75 = {94.972f, 100f, 122.638f};
	    public static float[] CIE2_F2 = {99.187f, 100f, 67.395f}; //Fluorescent
	    public static float[] CIE2_F7 = {95.044f, 100f, 108.755f};
	    public static float[] CIE2_F11 = {100.966f, 100f, 64.370f};
	    
	    //https://en.wikipedia.org/wiki/CIE_1964_color_space
	    // X2, Y2, Z2
	    public static float[] CIE10_A = {111.144f, 100f, 35.200f}; //Incandescent
	    public static float[] CIE10_C = {97.285f, 100f, 116.145f};
	    public static float[] CIE10_D50 = {96.720f, 100f, 81.427f};
	    public static float[] CIE10_D55 = {95.799f, 100f, 90.926f};
	    public static float[] CIE10_D65 = {94.811f, 100f, 107.304f}; //Daylight
	    public static float[] CIE10_D75 = {94.416f, 100f, 120.641f};
	    public static float[] CIE10_F2 = {103.280f, 100f, 69.026f}; //Fluorescent
	    public static float[] CIE10_F7 = {95.792f, 100f, 107.687f};
	    public static float[] CIE10_F11 = {103.866f, 100f, 65.627f};
	    
	    
public static float[] RGBtoYCbCr(int red, int green, int blue, YCbCrColorSpace colorSpace){
        
        float r = (float)red / 255;
        float g = (float)green / 255;
        float b = (float)blue / 255;
        
        float[] YCbCr = new float[3];
        float y,cb,cr;
        
        if (colorSpace == YCbCrColorSpace.ITU_BT_601) {
            y = (float)(0.299 * r + 0.587 * g + 0.114 * b);
            cb = (float)(-0.169 * r - 0.331 * g + 0.500 * b);
            cr = (float)(0.500 * r - 0.419 * g - 0.081 * b);
        }
        else{
            y = (float)(0.2215 * r + 0.7154 * g + 0.0721 * b);
            cb = (float)(-0.1145 * r - 0.3855 * g + 0.5000 * b);
            cr = (float)(0.5016 * r - 0.4556 * g - 0.0459 * b);
        }
        
        YCbCr[0] = (float)y;
        YCbCr[1] = (float)cb;
        YCbCr[2] = (float)cr;
        
        return YCbCr;
    }
}
